FROM python:3.9.0

ENV PYTHONUNBUFFERED=1

WORKDIR /app

RUN pip install --upgrade pip "poetry==1.5.1"
RUN poetry config virtualenvs.create false --local
COPY pyproject.toml poetry.lock ./
RUN poetry install

COPY mysite_122 .

CMD ["gunicorn", "mysite_122.wsgi:application", "--bind", "0.0.0.0:8000"]
