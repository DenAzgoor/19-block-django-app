from django.contrib.sitemaps import Sitemap
from .models import Articles


class BlogSiteMap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Articles.objects.filter(published_at__isnull=False).order_by("-published_at")

    def lastmod(self, obj: Articles):
        return obj.published_at
