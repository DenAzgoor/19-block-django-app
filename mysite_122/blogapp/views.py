from django.contrib.syndication.views import Feed
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView
from .models import Articles


class ArticlesListView(ListView):
    queryset = (
        Articles.objects
        .filter(published_at__isnull=False)
        .order_by("-published_at")
    )


class ArticlesDetailView(DetailView):
    model = Articles


class LatestArticlesFeed(Feed):
    title = "Blog articles (latest)"
    description = "Updates on changes and addition blog articles"
    link = reverse_lazy("blogapp:articles")

    def items(self):
        return (
            Articles.objects
            .filter(published_at__isnull=False)
            .order_by("-published_at")[:5]
        )

    def item_title(self, item: Articles):
        return item.title

    def item_description(self, item: Articles):
        return item.body[:200]

    # def item_link(self, item: Articles):
    #     return reverse("blogapp:article", kwargs={"pk": item.pk})
