# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-14 10:16+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: app_pages/templates/app_pages/greetings.html:5
msgid "What time is it?"
msgstr "Wie spät ist es?"

#: app_pages/templates/app_pages/translation_example.html:5
msgid "Our translation page title"
msgstr "Der Titel unserer Übersetzungsseite"

#: app_pages/templates/app_pages/translation_example.html:9
msgid "Translated text"
msgstr "Übersetzter Text"

#: app_pages/templates/app_pages/translation_example.html:20
msgid "Welcome to our page"
msgstr "Willkommen auf unserer Seite"

#: app_pages/views.py:13
#, python-format
msgid "Hello there! Today is %(date)s %(time)s"
msgstr "Hallo! Heute ist %(date)s %(time)s"

#: shopapp/apps.py:8
msgid "shop"
msgstr "geschäft"

#: shopapp/models.py:15
msgid "products"
msgstr "produkte"

#: shopapp/models.py:16
msgid "product"
msgstr "produkt"

#: shopapp/models.py:19
msgid "name"
msgstr "name"

#: shopapp/models.py:20
msgid "description"
msgstr "beschreibung"

#: shopapp/models.py:21
msgid "price"
msgstr "preis"

#: shopapp/models.py:22
msgid "discount"
msgstr "rabatt"

#: shopapp/models.py:23
msgid "created at"
msgstr "hergestellt in"

#: shopapp/models.py:24
msgid "archived"
msgstr "archiviert"

#: shopapp/models.py:25
msgid "preview"
msgstr "vorschau"
